﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPausa : MonoBehaviour
{
    public GameObject PantallaPausa;
    private bool ActivarMenu;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ActivarMenu = !ActivarMenu;
        }

        if (ActivarMenu == true)
        {
            PantallaPausa.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            PantallaPausa.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public void Reanuadar()
    {
        PantallaPausa.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        ActivarMenu = false;
    }
}
