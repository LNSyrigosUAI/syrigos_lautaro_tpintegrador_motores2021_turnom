﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlVictoria : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ControlGameOver.controlGameOver.LlamarWin();
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
