﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPuntuacion : MonoBehaviour
{
    public static int Puntuacion;
    Text text;

    void Awake()
    {
        text = GetComponent<Text>();
        Puntuacion = 0;
    }

    void Update()
    {
        text.text = "Score: " + Puntuacion;
    }
}
