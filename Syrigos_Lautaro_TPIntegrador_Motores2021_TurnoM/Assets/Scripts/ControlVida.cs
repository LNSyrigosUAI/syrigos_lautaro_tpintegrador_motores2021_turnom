﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlVida : MonoBehaviour
{
    ControlJugador _VidaJugador;//Para tener acceso y manipular la Vida del jugador.
    public int Cantidad;//La cantidad de vida que va a restar o sumar.
    public float TiempoDaño;//El tiempo que tardara en que ocurra el efecto de restar o sumar vida.
    float TiempoDañoActual;//Esta variable va a llevar la cuenta del tiempo en el que trancurre el tiempo de daño.

    void Start()
    {
        _VidaJugador = GameObject.FindWithTag("Player").GetComponent<ControlJugador>();//Para acceder a las propiedades del jugador.
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")//Si el jugador colisiona y/o esta en contacto con el GameObject que tenga el script, va a inicializar la funcion de rstar o sumar vida.
        {
            TiempoDañoActual += Time.deltaTime;//Para que el tiempo de daño actual sea en funcion del tiempo.
            if (TiempoDañoActual > TiempoDaño)//Si el tiempo de daño actual es mayor al tiempo de daño, va a ejecutar la funcion 
            {
                _VidaJugador.Vida += Cantidad;//Va a sumar la cantidad de vida que se establezca, en caso de que reste vida se modifica la cantidad en el inspector con un numero negativo.
                TiempoDañoActual = 0.0f;//Para que siempre el tiempo de daño empiece en cero.
            }
        }  
    }
}
