﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Transform PuertaObj;//Recibe la posicion de la puerta.
    public Transform PuertaAbierta;//Para poder mover la puerta hacia algun lugar y permita el paso del jugador.
    public Transform PuertaCerrada;//Para poder establecer donde la puerta va a estar bloqueada para evitar el paso del jugador.
    [Range(0,10)] public float VelocidadPuerta;
    Vector3 PosicionDestinada;//Para poder especificar a la puerta en que tiempo determinado se va a mover.
    float Tiempo;//Para saber en que puento entre abierto y cerrado va a estar la puerta. Va a inicializar con 0 hasta 1.
    public bool Bloqueada = true;

    void Start()
    {
        PosicionDestinada = PuertaCerrada.position;
    }

    void Update()
    {
        if (Bloqueada && PuertaObj.position != PosicionDestinada)
        {
            PuertaObj.position = Vector3.Lerp(PuertaObj.position, PosicionDestinada, Tiempo);//Para hacer una transicion entre distintas posiciones para la puerta.
            Tiempo += Time.deltaTime * VelocidadPuerta;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PosicionDestinada = PuertaAbierta.position;//Usando la variable Posicion Destinada ya no va a afectar a la posicion de la puerta, ya que con esta variable se va a especificar hacia donde va ir la puerta.
            Tiempo = 0;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            PosicionDestinada = PuertaCerrada.position;//Usando la variable Posicion Destinada ya no va a afectar a la posicion de la puerta, ya que con esta variable se va a especificar hacia donde va ir la puerta.
            Tiempo = 0;
        }
    }
}
