﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    CharacterController characterController;
    [Header("JUGADOR")]
    [Range(0, 10)] public int VelocidadCaminar;
    [Range(0, 10)] public int AlturaSalto;
    private float _ValorGravedad = -9.81f;
    [Header("CAMARA")]
    public Camera CamaraJugador;
    [Range(0, 10)] public float MouseHorizontal;
    [Range(0, 10)] public float MouseVertical;
    [Range(-100, 0)] public float RotacionMinima;
    [Range(0, 100)] public float RotacionMaxima;
    float _MouseH, _MouseV;
    private Vector3 Movimiento = Vector3.zero;
    [Header("VIDA DEL JUGADOR")]
    public float Vida = 100f;//Vida con la que inicia el jugador.
    public Image ControlVida;//La imagen que utiliza la Imagen de vida en el Panel del Canvas.

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        GestorAudio.instancia.ReproducirSonido("musica1");
    }

    void Update()
    {
        //MOVIMIENTOS DE LA CAMARA.

        _MouseH = MouseHorizontal * Input.GetAxis("Mouse X");
        _MouseV += MouseVertical * Input.GetAxis("Mouse Y");
        _MouseV = Mathf.Clamp(_MouseV, RotacionMinima, RotacionMaxima);
        CamaraJugador.transform.localEulerAngles = new Vector3(-_MouseV, 0, 0);
        transform.Rotate(0, _MouseH, 0);

        //MOVIMIENTOS DEL JUGADOR.

        if (characterController.isGrounded)
        {
            Movimiento = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            Movimiento = transform.TransformDirection(Movimiento) * VelocidadCaminar;
            if (Input.GetButton("Jump"))//SI SE PULSA LA TECLA ESPACIO EL JUGADOR PUEDE SALTAR.
                Movimiento.y = AlturaSalto;
        }
        Movimiento.y += _ValorGravedad * Time.deltaTime;
        characterController.Move(Movimiento * Time.deltaTime);

        //CONTROL DE LA VIDA DEL JUGADOR.

        Vida = Mathf.Clamp(Vida, 0, 100);//Se establece un minimo y un maximo que la vida no va a sobrepasar.
        ControlVida.fillAmount = Vida / 100;
        if (Vida <= 0)//Si la vida del jugador es menor o igual a 0 imprime la imagen de Game Over.
        {
            gameObject.SetActive(false);//Si esta en false no aparece la pantalla de Game Over.
            ControlGameOver.controlGameOver.LlamarGameOver();//Llama a la funcion de llamar de game over.
            Cursor.lockState = CursorLockMode.None;
        }

        //Lo que siga...
    }
}
