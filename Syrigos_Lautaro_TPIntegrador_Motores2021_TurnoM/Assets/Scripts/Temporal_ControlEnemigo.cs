﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temporal_ControlEnemigo : MonoBehaviour
{
    private int hp;
    public int ValorDaño = 10;
    public GameObject Llave;
    public Transform ZonaLoot;

    void Start()
    {
        hp = 100;
    }

    public void recibirDaño()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            this.desaparecer();

        }
    }

    private void desaparecer()
    {
        ControlPuntuacion.Puntuacion += ValorDaño;
        Instantiate(Llave,ZonaLoot);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            recibirDaño();
        }

    }

}

