﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlBot : MonoBehaviour
{
    [Header("RUTINA")]
    public float Rutina;//Para las rutinas del enemigo.
    public float Cronometro;//Para establecer el tiempo entre rutinas.
    public Animator animator;//Para las animaciones del enemigo.
    public Quaternion Angulo;//Para rotar al enemigo.
    public float Grado;//Para detectar el grado de Rotacion;
    public GameObject Objetivo;//Para establecer el objetivo del enemigo.
    public bool EstaAtacando;//Para establecer, si el enemigo esta atacando o no.
    public RangoDeAtaque rangoDeAtaque;//Para establecer donde va a atacar el enemigo.
    [Header("VIDA")]
    [Range(0, 100)] public int VidaEnemigo;
    [Range(0, 100)] public int DañoGolpe;
    [Header("LOOT")]
    public GameObject Llave;
    public Transform ZonaLoot;

    void Start()
    {
        animator = GetComponent<Animator>();//Para invocar al animator.
        Objetivo = GameObject.Find("Jugador");//Se establece que el G.O jugador va a ser el objetivo.
    }

    void Update()
    {
        ComportamientoEnemigo();
    }

    public void ComportamientoEnemigo()//Funcionamiento del cronometro.
    {
        if (Vector3.Distance(transform.position,Objetivo.transform.position)>5)//Si el jugador esta fuera del rango del enemigo, sigue su rutina de caminar y idle.
        {
            animator.SetBool("Correr", false);
            Cronometro += 1 * Time.deltaTime;

            if (Cronometro >= 4)
            {
                Rutina = Random.Range(0, 2);
                Cronometro = 0;
            }

            switch (Rutina)
            {
                case 0://El enemigo va a estar quieto.
                    animator.SetBool("Caminar", false);
                    break;
                case 1://Se determina la direccion hacia donde se desplazara.
                    Grado = Random.Range(0, 360);
                    Angulo = Quaternion.Euler(0, Grado, 0);
                    Rutina++;
                    break;
                case 2://El enemigo se va a estar movimiendo.
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Angulo, 0.5f);
                    transform.Translate(Vector3.forward * 1 * Time.deltaTime);
                    animator.SetBool("Caminar", true);
                    break;
            }
        }
        else//Si esta dentro del rango del enemigo, este ira a perseguir y atacar al jugador.
        {
            var PosicionMirar = Objetivo.transform.position - transform.position;
            PosicionMirar.y = 0;
            var Rotacion = Quaternion.LookRotation(PosicionMirar);
            
            if (Vector3.Distance(transform.position, Objetivo.transform.position) > 1 && !EstaAtacando)//Si esta el jugador cerca, el enemigo empezara a correr y atacarlo.
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Rotacion, 2);
                animator.SetBool("Caminar", false);

            animator.SetBool("Correr", true);
                transform.Translate(Vector3.forward * 1 * Time.deltaTime);//Al correr se desplaza a una velocidad mayor a la velocidad de caminar.
                animator.SetBool("Atacar", false);
            }
            else//Si el enemigo esta lo suficientemente cerca del jugador este se detendra y atacara al jugador.
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Rotacion, 2);
                animator.SetBool("Caminar", false);
                animator.SetBool("Correr", false);
            }
        }
    }

    public void TerminarAnimacion()//Se encargara de terminar las animaciones y el estado del enemigo cuando este atacando.
    {
        animator.SetBool("Atacar", false);
        EstaAtacando = false;
        rangoDeAtaque.GetComponent<CapsuleCollider>().enabled = true;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            VidaEnemigo -= DañoGolpe;
        }

        if (VidaEnemigo <= 0)
        {
            Instantiate(Llave, ZonaLoot);
            Destroy(gameObject);
        }
    }
}
