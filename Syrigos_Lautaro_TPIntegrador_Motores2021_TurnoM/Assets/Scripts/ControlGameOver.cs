﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGameOver : MonoBehaviour
{
    public GameObject PantallaGameOver;//Recibe la imagen que va a imprimir en pantalla.
    public GameObject PantallaWin;
    public static ControlGameOver controlGameOver;//Para poder acceder y manipular el Game Over desde otros scripts.

    void Start()
    {
        controlGameOver = this;//Para asignarle el control game over a este script.
    }

    public void LlamarGameOver()//Para poder invocar el Game Over.
    {
        PantallaGameOver.SetActive(true);//Si esta en true muestra la pantalla con el mensaje de Estas Muerto.
    }

    public void LlamarWin()
    {
        PantallaWin.SetActive(true);
    }
}
