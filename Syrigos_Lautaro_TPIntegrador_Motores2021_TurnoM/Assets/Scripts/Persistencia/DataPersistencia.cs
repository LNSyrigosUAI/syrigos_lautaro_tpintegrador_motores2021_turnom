﻿using System;
using UnityEngine;

[System.Serializable]
public class DataPersistencia
{
    public Punto posicionJugador;
}
[System.Serializable]
public class Punto
{
    public float x;
    public float y;
    public float z;

    public Punto(Vector3 p)
    {
        x = p.x; y = p.y; z = p.z;
    }
    public Vector3 aVector()
    {
        return new Vector3(x, y, z);
    }
}