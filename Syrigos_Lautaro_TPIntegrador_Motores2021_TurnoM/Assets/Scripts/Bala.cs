﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    [Range(0,10)] public float Duracion = 2f;
    float TiempoExistencia;
    
    private void Start()
    {
        TiempoExistencia = Duracion;
    }
    private void Update()
    {
        TiempoExistencia -= Time.deltaTime;
        if (TiempoExistencia <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
