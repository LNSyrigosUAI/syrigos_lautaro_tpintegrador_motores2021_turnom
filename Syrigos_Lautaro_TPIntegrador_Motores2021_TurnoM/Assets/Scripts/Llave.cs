﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llave : MonoBehaviour
{
    public Puerta AbrirPuerta;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AbrirPuerta.Bloqueada = true;
        }
        else if (other.tag == "Bullet")
        {
            DontDestroyOnLoad(gameObject);
        }

        Destroy(gameObject);
    }
}
