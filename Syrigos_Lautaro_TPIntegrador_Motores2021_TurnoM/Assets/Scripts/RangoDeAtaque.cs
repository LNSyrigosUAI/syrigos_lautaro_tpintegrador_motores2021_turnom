﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoDeAtaque : MonoBehaviour
{
    public Animator animatorDos;
    public ControlBot controlBot;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animatorDos.SetBool("Caminar", false);
            animatorDos.SetBool("Correr", false);
            animatorDos.SetBool("Atacar", true);
            controlBot.EstaAtacando = true;
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
