﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlArma : MonoBehaviour
{
    [Header("CONFIGURACION ARMA")]
    public int Daño;
    public float VelocidadDisparo, Propagacion, Alcance, TiempoRecarga, TiempoEntreDisparo;
    public int TamañoCargador, BalaPorDisparo;
    public bool MantenerPulsado;
    int BalaRestante, BalaDisparada;
    bool Disparando, DisparoListo, Recargando;
    [Header("REFERENCIAS")]
    public Camera CamaraArma;
    public Transform PuntoDisparo;
    public RaycastHit hit;
    public GameObject FlashCañon, Proyectiles;
    public TextMeshProUGUI Municion;

    private void Awake()
    {
        BalaRestante = TamañoCargador;
        DisparoListo = true;
    }

    private void Update()
    {
        InputArma();
        BalaRestante = Mathf.Clamp(BalaRestante, 0, TamañoCargador);//Para limitar la municion restante, y no de valores negativos.
        Municion.SetText(BalaRestante + " / " + TamañoCargador);//Se establece como va a aparecer la cantidad de municion en el juego.
    }

    private void InputArma()
    {
        if (MantenerPulsado)
        {
            Disparando = Input.GetButton("Fire1");
        }
        else
        {
            Disparando = Input.GetButtonDown("Fire1");
        }

        if (Input.GetKeyDown(KeyCode.R) && BalaRestante < TamañoCargador && !Recargando)
        {
            Recargar();
        } 

        if (DisparoListo && Disparando && !Recargando && BalaRestante > 0)
        {
            BalaDisparada = BalaPorDisparo;
            Disparar();
        }
    }

    private void Disparar()
    {
        DisparoListo = false;
        float x = Random.Range(-Propagacion, Propagacion);
        float y = Random.Range(-Propagacion, Propagacion);
        Vector3 Direccion = CamaraArma.transform.forward + new Vector3(x, y, 0);//Para calcular la direccion de donde se va a propagar las balas.

        if (Physics.Raycast(CamaraArma.transform.position, Direccion, out hit, Alcance))
        {
            Debug.Log(hit.collider.name);
            if (hit.collider.CompareTag("Enemy"))//Si el raycast toca al enemigo, llama a la funcion dentro del script del enemigo de recibir daño.
            { 
                hit.collider.GetComponent<Temporal_ControlEnemigo>().recibirDaño();
            }
        }
        Instantiate(Proyectiles, hit.point, Quaternion.Euler(0, 180, 0));
        //Destroy(Proyectiles, 2);//Para destruir las balas despues de 2 segundos.
        Instantiate(FlashCañon, PuntoDisparo.position, Quaternion.identity);
        
        BalaRestante--;
        BalaDisparada--;
        Invoke("ReiniciarDisparo", VelocidadDisparo);
        if (BalaDisparada > 0 && BalaRestante > 0)
        {
            Invoke("Disparar", TiempoEntreDisparo);
        }
    }

    private void ReiniciarDisparo()
    {
        DisparoListo = true;
    }

    private void Recargar()
    {
        Recargando = true;
        Invoke("RecargaTerminada", TiempoRecarga);
    }

    private void RecargaTerminada()
    {
        BalaRestante = TamañoCargador;
        Recargando = false;
    }
}
